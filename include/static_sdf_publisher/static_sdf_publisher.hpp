#pragma once


#include <rclcpp/node.hpp>
#include <sdf/sdf.hh>
#include <kdl/tree.hpp>
#include <tf2_ros/static_transform_broadcaster.h>


namespace static_sdf_publisher
{


class StaticSDFPublisher : public rclcpp::Node
{
private:
    tf2_ros::StaticTransformBroadcaster tf_broadcaster;
    rclcpp::TimerBase::SharedPtr timer;
    std::vector<geometry_msgs::msg::TransformStamped> transforms;

private:
    void publishStaticTransforms();

public:
    StaticSDFPublisher(sdf::SDFPtr sdf_ptr, std::string prefix = "", bool intra_process_comms = false);
};


} // namespace static_sdf_publisher

