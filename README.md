# Description
The `static_sdf_publisher` package is used to read fixed transforms from SDF files and publish these in
ROS2 using [TransformStamped](https://docs.ros.org/kinetic/api/geometry_msgs/html/msg/TransformStamped.html)
messages. Instead of runing the publisher node, it is also possible to link to the `static_sdf_publisher`
library to use the functionalities provided by this package.

# Run
```sh
ros2 run static_sdf_publisher static_sdf_publisher_node <path_to_sdf_file.sdf>
```

# static_sdf_publisher_node node
## Published topics
`tf_static` ([TransformStamped](https://docs.ros.org/kinetic/api/geometry_msgs/html/msg/TransformStamped.html))
    Static transforms between frames given by joints in the SDF file

# License
This Software is distributed under the [MIT License](https://opensource.org/licenses/MIT).
