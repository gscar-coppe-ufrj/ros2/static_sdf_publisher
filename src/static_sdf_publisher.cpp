#include "static_sdf_publisher/static_sdf_publisher.hpp"

#include <chrono>


using namespace ::static_sdf_publisher;


StaticSDFPublisher::StaticSDFPublisher(sdf::SDFPtr sdf, std::string prefix, bool intra_process_comms) :
    rclcpp::Node("static_sdf_publisher",
        rclcpp::NodeOptions().use_intra_process_comms(intra_process_comms)),
    tf_broadcaster(*this)
{
    declare_parameter("prefix", prefix);
    prefix = get_parameter("prefix").as_string();
    if (!prefix.empty())
        prefix += "/";

    using namespace std::chrono_literals;

    sdf::ElementPtr model = sdf->Root()->GetElement("model");
    if (!model)
    {
        RCLCPP_ERROR(get_logger(), "no root model in SDF file; static transforms will not be published");
        return;
    }

    using ignition::math::Pose3d;
    std::map<std::string, Pose3d> poses;

    auto link = model->GetElement("link");
    while (link)
    {
        std::string name = link->GetAttribute("name")->GetAsString();
        auto pose = link->Get<Pose3d>("pose", Pose3d()).first;
        poses[name] = pose;
        link = link->GetNextElement("link");
    }

    auto joint = model->GetElement("joint");
    while (joint)
    {
        if (joint->GetAttribute("type")->GetAsString() != "fixed")
            continue;

        std::string name_parent = joint->Get<std::string>("parent", "").first;
        std::string name_child  = joint->Get<std::string>("child", "").first;
        //
        auto pose_parent = poses[name_parent];
        auto pose_child  = poses[name_child];
        //
        Pose3d tf_pose = - pose_parent + pose_child;

        geometry_msgs::msg::Vector3 translation;
        geometry_msgs::msg::Quaternion rotation;
        //
        translation.x = tf_pose.Pos().X();
        translation.y = tf_pose.Pos().Y();
        translation.z = tf_pose.Pos().Z();
        //
        rotation.w = tf_pose.Rot().W();
        rotation.x = tf_pose.Rot().X();
        rotation.y = tf_pose.Rot().Y();
        rotation.z = tf_pose.Rot().Z();

        geometry_msgs::msg::TransformStamped tf;
        tf.header.frame_id = prefix + name_parent;
        tf.child_frame_id = prefix + name_child;
        tf.transform.translation = translation;
        tf.transform.rotation = rotation;
        transforms.push_back(tf);

        joint = joint->GetNextElement("joint");
    }

    timer = create_wall_timer(1s, std::bind(&StaticSDFPublisher::publishStaticTransforms, this));
}

void StaticSDFPublisher::publishStaticTransforms()
{
    for (auto &tf : transforms)
        tf.header.stamp = get_clock()->now();
    tf_broadcaster.sendTransform(transforms);
}
