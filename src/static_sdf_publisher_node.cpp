#include "static_sdf_publisher/static_sdf_publisher.hpp"


using namespace ::static_sdf_publisher;


int main(int argc, char** argv)
{
    // Initialize ROS and retrieve arguments
    std::vector<std::string> args = rclcpp::init_and_remove_ros_arguments(argc, argv);

    if (args.size() < 2 && args.size() > 3)
    {
        std::cerr << "static_sdf_publisher_node requires an SDF file\n" << std::endl;
        std::cerr << "Usage: static_sdf_publisher_node sdf_file_name [prefix]" << std::endl;
        return -1;
    }

    auto sdf = sdf::readFile(args[1]);
    if (!sdf)
    {
        std::cerr << "unable to open SDF file\n" << std::endl;
        return -1;
    }

    std::string prefix = (args.size() == 3) ? args[2] : "";
    auto node = std::make_shared<StaticSDFPublisher>(sdf, prefix);

    rclcpp::spin(node);
    return 0;
}
